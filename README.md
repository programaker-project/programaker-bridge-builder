# PrograMaker bridge builder

This repository contains the code and information necessary to replicate [PrograMaker.com's bridge builder](https://programaker.com).

## Setup

### Cluster

To setup a bridge builder you will need a Kubernetes cluster capable of running untrusted workloads in a secure manner. This can be done using [Kata Containers](https://katacontainers.io/), which wraps the containers being run in a Virtual Machine.

A way to get a Kubernetes cluster like this is to get a bare-metal machine which supports nested virtualization and run [kubespray](https://kubespray.io/) on it, applying the [configuration to use Kata Containers](https://github.com/kubernetes-sigs/kubespray/blob/master/docs/kata-containers.md).

For example, for a single machine this can be done following these steps:

 1. Get bare metal machine (needs root access). For this example we will use a Debian 10. Just keep in mind to replace `MY_METAL_MACHINE` with the IP address of your machine.
 2. Use SSH public key to authenticate as root on the machine. Run `ssh-copy-id root@MY_METAL_MACHINE` (you might have to generate a new key with `ssh-keygen` if none is found).
 3. Install `sudo` in the machine, this will be used by Kubespray's ansible scripts.

    3.1. `ssh root@MY_METAL_MACHINE apt update`
    3.2. `ssh root@MY_METAL_MACHINE apt install sudo`

 4. Check for nested virtualization support

     4.1. `ssh root@MY_METAL_MACHINE cat /sys/module/kvm_intel/parameters/nested`

     4.2. If the previous step didn't return a `Y` , nested virtualization is not supported. To enable it follow [this page](https://www.linux-kvm.org/page/Nested_Guests).

      4.3. If the nested virtualization __was__ not set, reboot the machine afterwards `ssh root@MY_METAL_MACHINE reboot`.

 5. Set the base configuration for [kubespray](https://kubespray.io/). For this you will need to clone [the kubespray repo](https://github.com/kubernetes-sigs/kubespray/) and follow the first steps on the README.

     5.1. `git clone https://github.com/kubernetes-sigs/kubespray/ && cd kubespray`

     5.2. Install dependencies `pip3 install --user -r requirements.txt`

    5.3. Create new configuration `cp -rfp inventory/sample inventory/mycluster`

    5.4. Declare the cluster IPs `declare -a IPS=(MY_METAL_MACHINE)`. This must be an IP (hostname won't work). Multiple IPs can be added inside the parens (separated by spaces) if necessary.

    5.5. Generate the configuration `CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}`.

 6. [Configure to use kata-containers](https://github.com/kubernetes-sigs/kubespray/blob/master/docs/kata-containers.md)

    6.1. On the file **inventory/mycluster/group_vars/k8s-cluster/k8s-cluster.yml** look for a line starting with `container_manager:` and set it to `container_manager: containerd`.

    6.2. (Same file) just below, change `kata_containers_enabled: false` to `kata_containers_enabled: true`

    6.3. (Same file) [add the following lines](https://github.com/kubernetes-sigs/kubespray/blob/master/docs/kata-containers.md#configuration)

      ```yaml
      kubelet_cgroup_driver: cgroupfs
      kata_containers_qemu_overhead: true
      kata_containers_qemu_overhead_fixed_cpu: 10m
      kata_containers_qemu_overhead_fixed_memory: 290Mi
      ```

     6.4. On the file **inventory/mycluster/group_vars/etcd.yml** look for a line starting as `etcd_deployment_type:` and set it to `etcd_deployment_type: host`.


 7. Complete inventory

      7.1. On the file **inventory/mycluster/hosts.yaml** update the `ansible_host: an.ip.addres` with `ansible_host: root@an.ip.address`

      7.2. On the file **inventory/mycluster/inventory.ini**, on the `[all]` section add an entry like `node1 ansble_host=root@MY_METAL_SERVER`. Which `node` is assigned to each IP can be found on the **hosts.yaml*** file.

      7.3. (Still on **inventory.ini**) add the the nodes to the `[categories]`, just add a `nodeX` on a newline on each category which the node fits in. For example, if we have a single node cluster, the file will be like this:

    ```yaml
    # ## Configure 'ip' variable to bind kubernetes services on a
    # ## different ip than the default iface
    # ## We should set etcd_member_name for etcd cluster. The node that is not a etcd member do not need to set the value, or can set the empty string value.
    [all]
    node1 ansible_host=root@MY_METAL_NODE

    # ## configure a bastion host if your nodes are not directly reachable
    # [bastion]
    # bastion ansible_host=x.x.x.x ansible_user=some_user

    [kube-master]
    node1

    [etcd]
    node1

    [kube-node]
    node1

    [calico-rr]

    [k8s-cluster:children]
    kube-master
    kube-node
    calico-rr
    ```

 8. Launch kubespray: `ansible-playbook -i inventory/mycluster/hosts.yaml  --become --become-user=root cluster.yml`

 9. Get KUBECONFIG file

     9.1. `scp root@MY_METAL_SERVER:/etc/kubernetes/admin.conf kubespray-do.conf`

    9.2. Edit the **kubespray-do.conf** file and replace `server: https://127.0.0.1:6443` with `server: https://MY_METAL_SERVER:6443`

    9.3. Test access: `KUBECONFIG="$(pwd)/kubespray-do.conf" kubectl get pods -n kube-system`. If this worked, it should show some system pods.

### Restrict resource usage

Kata-containers can be configured to give the VMs it creates a specific RAM or CPU count. The allocated values are calculated as a default baseline + the limits given to each pod.

We can use this to limit the resources that an untrusted bridge can use. To configure the defaults edit the `/etc/kata-containers/configuration-qemu.toml` on the server. For example with `ssh -t root@MY_METAL_SERVER nano /etc/kata-containers/configuration-qemu.toml`.

Inside that file, we can find the RAM allocation baseline (as MB) on the entry `default_memory`, we will set it to 256M:

  - `default_memory = 256`

The CPU count is set through `default_vcpus`, we will leave the default `1`. Note that this number must be an integer (so no fractional CPU counts is allowed).

  - `default_vcpus = 1`

After this, all the PODs created with `runtimeClassName: kata-qemu` will take this limits as baseline.

If we want to **increase** that limits for a given POD or deployment, we can just add the difference to the `limits` entry on the `resources` map. For example, the following would add 512M RAM and 1 extra CPU to the pods of a deployment:

```yaml
    spec:
      containers:
      - image: k8s.gcr.io/hpa-example
        # ...
        resources:
          limits:
            cpu: "1"
            memory: 512M
```


### Network policies

[Kubernetes's network policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
are used to make sure that the bridges hosted on the bridge-builder infrastructure can only connect to external services or to limited internal ones. Specifically, the connections will be allowed to:

 - Any external IP on TCP ports 80 or 443 (policy: `allow-egress-to-public-ips`)
 - The internal DNS on UDP port 53 (policy: `allow-egress-to-kube-dns`)
 - The DNS confiigured by default on the pods `/etc/resolv.conf` on UDP and TCP port 53 (policy: `allow-egress-to-etc-resolv`)
 - The Postgres database for container usage on TCP port 5432 (policy: `kube-dns`)
 - All others are denied (policy: `default-deny-all-egress`)

To do this, follow these steps:

1. On folder **services/00-network-policies** `cd services/00-network-policies`
2. Label namespace with kube-dns pods: `kubectl label namespaces kube-system name=kube-system`
3. Apply all static network policies: `for i in *.yaml;do kubectl apply -f $i;done`
4. Allow DNS connection to the address on **/etc/resolv.conf**: `./10-allow-egress-to-etc-resolv.sh`
5. Go back: `cd ../..`

### Auxiliary services


Some services should be made available to the elements inside the bridge builder so it can perform it's functions. These services will live on the **aux** namespace:

1. Create namespace: `kubectl create ns aux`
2. Label namespace: `kubectl label namespaces aux name=aux`
3. On folder **services/01-aux-ingress**: `cd services/01-aux-ingress`
4. Run setup script: `./00-setup.sh`
5. On folder **services/02-cert-manager**: `cd ../02-cert-manager`
6. Run setup script: `./00-setup.sh`
7. Go back `cd ../..`

#### Container image registry

This container registry will hold the images of the bridges being deployed

1. On folder **services/03-container-registry/**: `cd services/03-container-registry/`
2. Generate a password for the container registry: `./00-create-secret.sh`
3. Deploy configuration: `kubectl apply -f 01-service.yaml -f 02-ingress.yaml -f 03-configmap.yaml -f 04-deployment.yaml`
4. Go back: `cd ../..`

#### Container database

This database can be used by the bridges to store user information or anything related. The address and credentials to connect to it can be found on the `CONNECTION_STRING` bridge's environment variable:

1. On folder **services/04-container-db/**: `cd services/04-container-db/`
2. Create root password: `./00-create-secret.sh`
3. Deploy configuration: `kubectl apply -f 01-service.yaml -f 02-persistent-volume.yaml -f 03-persistent-volume-claim.yaml -f 04-statefulset.yaml`
4. Go back: `cd ../..`

#### Manual push script

As a way to test this whole deployment, a script is provided to build and launch a test bridge that makes use of both the [container image registry](#container-image-registry) and the [bridge database](#bridge-database)

1. On folder **manual-push**: `cd manual-push`
2. Create a `config.sh` file with the bridge endpoint and token: `nano config.sh`

    ```bash
    ENDPOINT="wss://MY_PROGRAMAKER_INSTANCE/api/v0/bridges/by-id/MY_BRIDGE_ID"
    TOKEN="MY_TOKEN"
    ```

3. Launch script: `./build-and-push.sh`
