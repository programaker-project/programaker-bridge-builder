#!/usr/bin/env bash

set -eu

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

helm install aux-ingress ingress-nginx/ingress-nginx -f values.yaml --namespace aux
