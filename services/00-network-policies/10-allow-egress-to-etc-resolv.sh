#!/usr/bin/env bash

set -eu

IP=$(kubectl -n kube-system get configmap nodelocaldns  -o yaml |
         grep -P '^ *bind *\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' |
         head -n 1 |
         awk '{ print  $2; } '
     )

echo "IP: $IP"

cat <<EOF > 11-allow-egress-to-etc-resolv.yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: allow-egress-to-etc-resolv
  namespace: default
spec:
  podSelector: {}
  policyTypes:
  - Egress
  egress:
  - to:
    - ipBlock:
        cidr: ${IP}/32
    ports:
    - port: 53
      protocol: TCP
    - port: 53
      protocol: UDP

EOF

kubectl apply -f 11-allow-egress-to-etc-resolv.yaml
