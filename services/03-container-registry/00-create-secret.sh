#!/bin/sh

SECRET_NAME=container-registry-auth
SECRET_NAMESPACE=aux

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 2>&1 1>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

set -eu

PASSWORD=`openssl rand -base64 42|tr -d '='`

htpasswd -bBc auth bridges "$PASSWORD"

echo "# Creating container-registry auth..."
kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" --from-file=auth
rm auth

echo "# Generating cluster authentication secret..."
kubectl create secret docker-registry regcred --docker-server=bridge-registry.aux.programaker.com --docker-username=bridges --docker-password="$PASSWORD"

echo "# Generating kaniko secret..."
kubectl get secret regcred -o json |
    jq '.data[".dockerconfigjson"]' -r |
    base64 -d |
    jq > kaniko-secret.json

kubectl create secret generic kaniko-docker-registry-auth --from-file=config.json=kaniko-secret.json
rm kaniko-secret.json
