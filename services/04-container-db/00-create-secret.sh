#!/bin/sh

SECRET_NAME=container-database
SECRET_NAMESPACE=aux

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 2>&1 1>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

set -eu

ROOT_PASSWORD=`openssl rand -base64 42|tr -d '='`

kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" --from-literal=root-password="${ROOT_PASSWORD}"
