import os
import random

from programaker_bridge import BlockArgument  # Needed for argument definition
from programaker_bridge import ProgramakerBridge  # Import bridge functionality
from programaker_bridge.storage import Storage

STORAGE = Storage('NUMBER_SAVER')

# Create the bridge object
bridge = ProgramakerBridge(
    name="Number saver bridge",
    # Configure the bridge endpoint
    endpoint=os.environ["ENDPOINT"],
    # Configure the bridge token
    token=os.environ["TOKEN"],
)

# Define the getter
@bridge.getter(
    id="get_number",  # Give it an ID
    message="Get the saved number",  # Define the message on the block
    arguments=[
    ],
    block_result_type=int,  # The result is another integer
)
def get_random_number(extra_data):
    with STORAGE.on_user(extra_data.user_id) as user:
        return user['number']

@bridge.operation(
    id="save_number",
    message="Save the number %1",
    arguments=[
        BlockArgument(int, "7"),
    ]
)
def save_number(number, extra_data):
    with STORAGE.on_user(extra_data.user_id, autocreate=True) as user:
        user['number'] = number

bridge.run()  # Launch the bridge
