#!/usr/bin/env bash

set -eu

DIR="$(dirname "$0")/test-bridge"
cd $(dirname "$0")

BRIDGE_ID=manual-test-bridge

if [ -f "config.sh" ];then
    source config.sh
fi

ERROR=0
if [ -z "${ENDPOINT}" ];then
    echo "[ERROR] No ENDPOINT environment variable"
    ERROR=1
fi

if [ -z "${TOKEN}" ];then
    echo "[ERROR] No TOKEN environment variable"
    ERROR=1
fi

if [ $ERROR -eq 1 ];then
    exit 1
fi

UPLOAD_ID="$(uuidgen)"

JOB_ID="${BRIDGE_ID}-builder-${UPLOAD_ID}"
echo "Builder job: ${JOB_ID}"
cat <<EOF > "job-${JOB_ID}.tmp.yaml"
apiVersion: batch/v1
kind: Job
metadata:
  name: ${JOB_ID}
spec:
  backoffLimit: 0
  template:
    spec:
      runtimeClassName: kata-qemu
      initContainers:
      - name: loader
        image: alpine
        command: [ 'sh', '-eu', '-c', 'i=0; while [ ! -f /ready ];do sleep 1; i=\$(( \$i + 1 )); echo "\${i}s";done'  ]
        volumeMounts:
        - name: shared-data
          mountPath: /loaded
      containers:
      - name: kaniko
        image: gcr.io/kaniko-project/executor:latest
        args:
        - "-v=info"
        - "--single-snapshot"
        - "--snapshotMode=redo"
        - "--use-new-run"
        - "--context=dir:///loaded/"
        - "--destination=bridge-registry.aux.programaker.com/${BRIDGE_ID}:${UPLOAD_ID}"
        volumeMounts:
        - name: shared-data
          mountPath: /loaded
        - name: kaniko-secret
          subPath: config.json
          mountPath: /kaniko/.docker/config.json
        resources:
          limits:
            memory: 256M
      restartPolicy: Never
      volumes:
      - name: shared-data
        emptyDir: {}
      - name: kaniko-secret
        secret:
          secretName: kaniko-docker-registry-auth
EOF

kubectl create -f "job-${JOB_ID}.tmp.yaml"
rm "job-${JOB_ID}.tmp.yaml"
ORIG="$(pwd)"

cd "$DIR"
POD_ID=$(kubectl get pods -l job-name="${JOB_ID}" -o json|jq .items[0].metadata.name -r)
while [ -z "${POD_ID}" ] || [ "${POD_ID}" == "null" ];do
    echo "Waiting for job..."
    sleep 1
    POD_ID=$(kubectl get pods -l job-name="${JOB_ID}" -o json|jq .items[0].metadata.name -r)
done

## Upload files
# TODO: Properly wait for Init phase
set +e
kubectl exec ${POD_ID} -c loader -- true
while [ $? -ne 0 ];do
    echo "Waiting for Init container..."
    sleep 1
    kubectl exec ${POD_ID} -c loader -- true
done
set -e

echo -n "Uploading data..."
## Workaround EOF on kata-containers
# This is only necessary if the job is `runtimeClassName: kata-qemu`, for others
# `kubectl cp` should be enough
tmp=`mktemp -t XXXXXXXX.tar`
tar cf "$tmp" .
size=`wc -c "$tmp" | cut -d\  -f1`
cat "$tmp" | kubectl exec -i "${POD_ID}" -c loader -- ash -c "head -c $size | tar x -C /loaded  ; exit 0"
rm "$tmp"
cd "$ORIG"

echo -n " OK, completing..."
# Mark completion
kubectl exec "${POD_ID}" -c loader -- touch /ready

echo " [DONE]"
echo "Waiting for logs..."
kubectl wait --for=condition=Ready --timeout=2m pod "${POD_ID}"
echo '---[Logs]------8<-------'
kubectl logs -f "${POD_ID}"

while [ `kubectl get pod "${POD_ID}" -o jsonpath="{.status.phase}"` != "Succeeded" ];do
    if  [ `kubectl get pod "${POD_ID}" -o jsonpath="{.status.phase}"` == "Failed" ];then
        echo "[ERROR] Error on builder" >&2
        exit 1
    fi
    echo "Waiting for completion..."
    sleep 1
done

# Image pushed, let's just prepare the auxiliary elements before deploying

echo "Creating secret and database..."
set +e
kubectl get secret "bridge-${BRIDGE_ID}" 2>/dev/null 1>/dev/null
x=$?
set -e
if [ $x -ne 0 ];then
    USER_PASSWORD=`openssl rand -base64 42|tr -d '='`
    USER_NAME="bridge-${BRIDGE_ID}"

    QUERY1="CREATE USER \"${USER_NAME}\" with password '${USER_PASSWORD}'"
    QUERY2="CREATE DATABASE \"${USER_NAME}\" WITH OWNER=\"${USER_NAME}\";"

    kubectl exec -n aux -i container-database-0 -- psql --user postgres -c "${QUERY1}" -c "${QUERY2}"

    BRIDGE_DB_PATH="postgresql://${USER_NAME}:${USER_PASSWORD}@container-database.aux/${USER_NAME}"

    kubectl create secret generic "bridge-${BRIDGE_ID}" --from-literal=endpoint="$ENDPOINT" \
            --from-literal=token="$TOKEN" \
            --from-literal=connection_string="${BRIDGE_DB_PATH}"
fi

## Everything is ready, now we can deploy it

echo "Running bridge..."
DEPLOYMENT_ID="${BRIDGE_ID}-${UPLOAD_ID}"
echo "Deployment: ${DEPLOYMENT_ID}"
cat <<EOF > "deployment-${DEPLOYMENT_ID}.tmp.yaml"
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${BRIDGE_ID}
  labels:
    bridge: ${BRIDGE_ID}

spec:
  replicas: 1
  selector:
    matchLabels:
      bridge: ${BRIDGE_ID}
  template:
    metadata:
      labels:
        bridge: ${BRIDGE_ID}
        upload: ${UPLOAD_ID}
    spec:
      runtimeClassName: kata-qemu
      containers:
      - name: bridge
        image: bridge-registry.aux.programaker.com/${BRIDGE_ID}:${UPLOAD_ID}
        imagePullPolicy: IfNotPresent
        env:
        - name: TOKEN
          valueFrom:
            secretKeyRef:
              name: bridge-${BRIDGE_ID}
              key: token
        - name: ENDPOINT
          valueFrom:
            secretKeyRef:
              name: bridge-${BRIDGE_ID}
              key: endpoint
        - name: CONNECTION_STRING
          valueFrom:
            secretKeyRef:
              name: bridge-${BRIDGE_ID}
              key: connection_string

      imagePullSecrets:
      - name: regcred

EOF
kubectl apply -f "deployment-${DEPLOYMENT_ID}.tmp.yaml"

rm "deployment-${DEPLOYMENT_ID}.tmp.yaml"
